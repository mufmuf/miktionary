from datetime import datetime, timezone
from pathlib import Path as Pathlib
from typing import Annotated

from db import crud, schemas
from db.database import Base, SessionLocal, engine
from fastapi import Depends, FastAPI, File, Form, Request, UploadFile, Query, Path
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session
from sqlalchemy.sql import text
from os import path, rename

RECORDINGS_DIR = "static/audio"
Pathlib(RECORDINGS_DIR).mkdir(parents=True, exist_ok=True)

Base.metadata.create_all(bind=engine)

app = FastAPI()

app.mount(
    "/static",
    StaticFiles(directory=path.join(path.dirname(__file__), "static/")),
    name="static",
)
templates = Jinja2Templates(directory=path.join(path.dirname(__file__), "templates/"))


# Dependency
def get_db():
    db = SessionLocal()
    db.execute(text("pragma foreign_keys=ON"))
    db.commit()
    try:
        yield db
    finally:
        db.close()


@app.post("/entries")
def create_dictionary_item(
    text: Annotated[str, Form()],
    # bytes somehow does not work here when we use both Form and File
    # therefore we use UploadFile
    recording: Annotated[UploadFile, File()],
    db: Session = Depends(get_db),
) -> schemas.DictionaryItem:
    # TODO validate the file to be uploaded
    # size, mime-type etc.

    now = datetime.now(tz=timezone.utc)
    encoded_text = ""
    for c in text.lower():
        if c.isalnum():
            encoded_text += c
        if c == " " or c == "_" or c == "-":
            encoded_text += "_"
    if encoded_text[-1] == "_":
        encoded_text = encoded_text[:-1]
    if len(encoded_text) == 0:
        encoded_text = "unnamed"
    filename = f"{RECORDINGS_DIR}/{now.strftime('%Y%m%d_%H%M%S')}-{encoded_text}"
    with Pathlib(filename).open("wb") as buffer:
        buffer.write(recording.file.read())

    item = schemas.BaseDictionaryItem(
        text=text,
        created_at=now,
        recordings=[
            schemas.BaseRecording(
                url=filename, content_type=recording.content_type, created_at=now
            )
        ],
    )

    return crud.create_dictionary_item(db=db, item=item)


@app.get("/", response_class=HTMLResponse)
def list_dictionary_items(
    request: Request, db: Session = Depends(get_db), page: int = Query(1, gt=0)
):

    return templates.TemplateResponse(
        "list_items.html.j2",
        {
            "request": request,
            "dictionary_items": crud.read_dictionary_items(db, page),
            "page": page,
        },
    )


@app.get("/entries/new", response_class=HTMLResponse)
def new_dictionary_item(request: Request):
    return templates.TemplateResponse(
        "new_item.html.j2",
        {"request": request},
        headers={"Permissions-Policy": "microphone=(self)"},
    )


@app.delete("/entries/{entry_id}")
def delete_dictionary_item(db: Session = Depends(get_db), entry_id: int = Path()):
    rows = crud.read_dictionary_item(db, entry_id)
    if len(rows) == 0:
        raise Exception(
            f"No dictionary item with id {entry_id} or no recordings associated with it"
        )
    recordings_urls = [row[1].url for row in rows]
    for url in recordings_urls:
        rename(url, f"{url}.DELETED")
    return crud.delete_dictionary_item(db, entry_id)
