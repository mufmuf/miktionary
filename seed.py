from db.database import SessionLocal
from sqlalchemy.orm import Session
from db.models import DictionaryItem, Recording
from datetime import datetime

db: Session = SessionLocal()

data = [
    DictionaryItem(source="Wafadt",
    translation="Laufrad", 
    recording=[Recording(
        url="assets/audio/laufrad_2023_09.mp3",
        content_type="audio/mp3",
        created_at=datetime()
    )])
]

db.add_all(data)
db.commit()