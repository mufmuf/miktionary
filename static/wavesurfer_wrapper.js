const wavesurferSetup = async () => {
  const inactiveColor = "rgba(152, 63, 42, 0.5)";
  const activeColor = "rgba(52, 183, 102, 0.5)";
  const WAVE_COLOR = "#4F4A85";
  const PROGRESS_COLOR = "#383351";
  const WS_CONTAINER = "#waveform";

  // mostly taken from the Wavesurfer.Record plugin
  // had to adapt the order of MIME_TYPE, so that clicking on a region works as expected
  // previously webm had more priority and was causing a problem
  // regarding clicking on a region to play only that region
  const addRecorder = async (state) => {
    let isRecording = false;
    const MIME_TYPES = [
      "audio/ogg",
      "audio/wav",
      "audio/mpeg",
      "audio/mp4",
      "audio/mp3",
      "audio/webm",
    ];
    const findSupportedMimeType = () => {
      const f = MIME_TYPES.find((mimeType) =>
        MediaRecorder.isTypeSupported(mimeType)
      );
      return f;
    };
    let stream;
    try {
      stream = await navigator.mediaDevices.getUserMedia({
        audio: true,
      });
    } catch (err) {
      throw new Error("Error accessing the microphone: " + err.message);
    }
    const mediaRecorder = new MediaRecorder(stream, {
      mimeType: findSupportedMimeType(),
      audioBitsPerSecond: 128000,
    });
    let recordedChunks = [];

    mediaRecorder.ondataavailable = (event) => {
      if (event.data.size > 0) {
        recordedChunks.push(event.data);
      }
    };

    mediaRecorder.onstop = () => {
      const soundData = new Blob(recordedChunks, {
        type: mediaRecorder.mimeType,
      });
      if (state.ws instanceof WaveSurfer) {
        state.ws.destroy();
      }
      const cropEl = document.getElementById('crop')
      if (cropEl) {
        cropEl.remove();
      }
      state.ws = WaveSurfer.create({
        container: WS_CONTAINER,
        waveColor: WAVE_COLOR,
        progressColor: PROGRESS_COLOR,
        url: URL.createObjectURL(soundData),
      });
      // Rewind to the beginning on finished playing
      state.ws.on("finish", () => {
        state.ws.setTime(0);
      });

      state.ws.on("decode", () => {
        addCropRegion(state, soundData);
      });
      state.soundData = soundData;
      recordedChunks = [];
    };

    const recButton = document.querySelector("#record");

    recButton.onclick = () => {
      if (isRecording) {
        mediaRecorder.stop();
        recButton.textContent = "Record";
        isRecording = false;
        return;
      }
      recButton.disabled = true;
      isRecording = true;

      mediaRecorder.start();
      recButton.textContent = "Stop";
      recButton.disabled = false;
    };
  };

  const addAudioUploader = (state) => {
    document
      .getElementById("audioUpload")
      .addEventListener("change", (event) => {
        const soundData = event.target.files[0];
        // document.getElementById('waveform').innerHTML = ''
        if (state.ws instanceof WaveSurfer) {
          state.ws.destroy();
        }
        state.ws = WaveSurfer.create({
          container: WS_CONTAINER,
          waveColor: WAVE_COLOR,
          progressColor: PROGRESS_COLOR,
          url: URL.createObjectURL(soundData),
        });
        state.ws.on("decode", () => {
          addCropRegion(state, soundData);
        });
        state.soundData = soundData;
      });
  };

  // https://github.com/lubenard/simple-mp3-cutter/
  // I guess we had to encode the cropped data again,
  // otherwise the cropped audio was losing quality with every crop
  const cropAudio = async (file, start, end) => {
    const buffer = await new Response(file).arrayBuffer();
    const audioContext = new AudioContext();
    const decodedData = await audioContext.decodeAudioData(buffer);
    const bitrate = 320;
    //Compute start and end values in secondes
    const computedStart = (decodedData.length * start) / decodedData.duration;
    const computedEnd = (decodedData.length * end) / decodedData.duration;

    //Create a new buffer
    const newBuffer = audioContext.createBuffer(
      decodedData.numberOfChannels,
      computedEnd - computedStart,
      decodedData.sampleRate
    );

    // Copy from old buffer to new with the right slice.
    // At this point, the audio has been cut
    for (var i = 0; i < decodedData.numberOfChannels; i++) {
      newBuffer.copyToChannel(
        decodedData.getChannelData(i).slice(computedStart, computedEnd),
        i
      );
    }

    // Bitrate is  by default 192, but can be whatever you want
    // const encoder = new Mp3LameEncoder(newBuffer.sampleRate, bitrate);

    const encoder = new OggVorbisEncoder(newBuffer.sampleRate, decodedData.numberOfChannels, 0.8);

    const channels = Array.apply(null, {
      length: newBuffer.numberOfChannels,
    }).map((_, i) => newBuffer.getChannelData(i));

    //Encode into mp3
    encoder.encode(channels);

    //When encoder has finished
    const compressed_blob = encoder.finish();

    return compressed_blob;
  };

  const addCropRegion = (state, soundData) => {
    const ws = state.ws;
    const wsRegions = ws.registerPlugin(WaveSurfer.Regions.create());
    let regionClicked = false;
    const region = wsRegions.addRegion({
      start: 0.3,
      end: 0.7,
      color: inactiveColor,
      drag: true,
      resize: true,
    });

    wsRegions.on("region-clicked", (region, e) => {
      e.stopPropagation();
      regionClicked = true;
      region.play();
      region.setOptions({ color: activeColor });
    });

    wsRegions.on("region-out", (region) => {
      if (regionClicked) {
        ws.pause();
        regionClicked = false;
        region.setOptions({ color: inactiveColor });
      }
    });

    // Play on click
    ws.on("interaction", () => {
      ws.setTime(0);
      regionClicked = false;
      ws.play();
    });

    // Rewind to the beginning on finished playing
    ws.on("finish", () => {
      ws.setTime(0);
    });

    const cropButton = document
      .querySelector(WS_CONTAINER)
      .insertAdjacentElement("afterend", document.createElement("button"));
    cropButton.textContent = "Crop";
    cropButton.id = "crop";
    cropButton.className +=
      " p-4 sm:py-2 hover:text-red-700 hover:bg-orange-200 bg-red-700 text-orange-200 rounded border border-red-700";
    cropButton.onclick = async () => {
      const croppedAudio = await cropAudio(soundData, region.start, region.end);
      ws.destroy();
      state.ws = WaveSurfer.create({
        container: WS_CONTAINER,
        waveColor: WAVE_COLOR,
        progressColor: PROGRESS_COLOR,
        url: URL.createObjectURL(croppedAudio),
      });
      state.ws.on("decode", () => {
        addCropRegion(state, croppedAudio);
      });
      state.soundData = croppedAudio;
      // remove itself from DOM
      const cropEl = document.getElementById('crop')
      if (cropEl) {
        cropEl.remove();
      }
    };
  };

  const state = {
    ws: undefined,
    soundData: undefined,
  };

  // addAudioUploader(state);
  await addRecorder(state);

  return state;
};
