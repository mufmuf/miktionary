from typing import List

from db import models, schemas
from sqlalchemy.orm import Session
from sqlalchemy import select, delete


def read_dictionary_items(
    db: Session, page: int = 1, limit: int = 10
) -> List[models.DictionaryItem]:
    # TODO: return joined recording table as well without breaking the list route
    return db.scalars(
        select(models.DictionaryItem)
        .order_by(models.DictionaryItem.created_at.desc())
        .offset((page - 1) * limit)
        .limit(limit)
    ).all()


def read_dictionary_item(db: Session, item_id: int):
    return db.execute(
        select(models.DictionaryItem, models.Recording)
        .join(models.DictionaryItem.recordings)
        .where(models.DictionaryItem.id == item_id)
    ).all()


def create_dictionary_item(
    db: Session, item: schemas.BaseDictionaryItem
) -> models.DictionaryItem:
    recordings = item.recordings
    item.recordings = []
    model_item = models.DictionaryItem(**item.model_dump())
    db.add(model_item)
    db.commit()
    db.refresh(model_item)

    model_recordings = [
        models.Recording(**recording.model_dump(), dictionary_item_id=model_item.id)
        for recording in recordings
    ]
    db.add_all(model_recordings)
    db.commit()

    # somehow model_item has the recordings list hydrated even without the below line
    # db.refresh(model_item)
    return model_item


def delete_dictionary_item(db: Session, item_id: int):
    db.execute(delete(models.DictionaryItem).where(models.DictionaryItem.id == item_id))
    db.commit()
    return
