from datetime import datetime
from pathlib import Path
from pydantic import BaseModel, field_serializer
from typing import List


class BaseRecording(BaseModel):
    url: Path
    created_at: datetime
    content_type: str

    @field_serializer("url")
    def serialize_url(self, url: Path, _info):
        return str(url)

class Recording(BaseRecording):
    id: int
    dictionary_item_id: int

    class Config:
        from_attributes = True


class BaseDictionaryItem(BaseModel):
    text: str
    created_at: datetime
    recordings: List[BaseRecording]


class DictionaryItem(BaseDictionaryItem):
    id: int
    recordings: List[Recording]

    class Config:
        from_attributes = True
