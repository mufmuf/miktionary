drop table dictionary_items;
drop table recordings;
delete from alembic_version;
delete from SQLITE_SEQUENCE where name='dictionary_items' or name='recordings';